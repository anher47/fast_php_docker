# fast_php_docker

A fast way to set up nginx server with php.

## To run the server:
~~~~
docker-compsoe build
~~~~

then:
~~~~
docker-compose run --service-ports app
~~~~

## finally, you can browse to localhost/[your php file name].


##### Feel free to ask for help.
